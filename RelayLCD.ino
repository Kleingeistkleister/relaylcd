
#include <LiquidCrystal.h>
#include <LCDKeypad.h>

LCDKeypad lcd;


int r1  =    53;
int r2  =    52;
int r3  =    51;
int r4  =    50;
int r5  =    49;
int r6  =    48;
int r7  =    47;
int r8  =    46;
int r9  =    45;
int r10 =    44;
int r11 =    43;
int r12 =    42;
int r13 =    41;
int r14 =    40;
int r15 =    38;
int r16 =    39;

//pinArray
int pinList[16] = {r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16};

//  moduswahl
int modus = 1;
//schaltgeschwindigkeit
int speedVal = 25;

//strings fuer print
String modusV;
String speedV;

//run abrgrage
boolean runProgram = false;
boolean allAreOn = false;


//lcd timer
long timer = 0;
long timeout = 100;


void setup() {
  lcd.begin(16, 2);
  lcd.clear();
  lcd.print("Relay Control");
  delay(1000);
  lcd.clear();
  //relay pin setup
  relaySetup();

  //press select to start main program
  while (lcd.button() != KEYPAD_SELECT) {
    lcd.clear();
    lcd.print("press select");
    delay(100);
  }
  printLCD();
  delay(1000);
}



void loop() {

  //ueberprüft die geschwindigkeit mit left / right
  //right schneller left langsamer
  checkSpeed();


  switch (menuButton()) {
    //-------------------------------------------------------------------------------------
    case 1:
      //startet ein programm wenn Select gedrueckt wird. erneutes druecken stoppt das programm.
      if (lcd.button() == KEYPAD_SELECT) {
        printLCD();
        delay(400);
        runProgram = true;
        //startet programm 1
        }
        program1();
      
      break;
    //-------------------------------------------------------------------------------------
    case 2:
      if (lcd.button() == KEYPAD_SELECT) {
        printLCD();
        delay(300);
        runProgram = true;
        }//if
        //startet programm 2
        program2();
    
      
      break;

    //-------------------------------------------------------------------------------------
    case 3:
      if (lcd.button() == KEYPAD_SELECT) {
        printLCD();
        delay(300);
        runProgram = true;
      }//if
        program3();
       
      
      break;

    //-------------------------------------------------------------------------------------
    case 4:
      if (lcd.button() == KEYPAD_SELECT) {
        program4();
      }
      break;
    //-------------------------------------------------------------------------------------
    default:
      break;
    //-------------------------------------------------------------------------------------
  }//switch case 2

  delay(50);
  //allOff();
  printLCD();
  runProgram = false;
}//mainloop
