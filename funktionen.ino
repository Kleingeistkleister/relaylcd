


//-------------------------------------------------------------------------------------
//----------------------------------  FUNKTIONEN   ------------------------------------
//-------------------------------------------------------------------------------------

void relaySetup() {
  for (int i = 0; i < 16; i++) {
    pinMode(pinList[i], OUTPUT);
  }
  for (int i = 0; i < 16; i++) {
    digitalWrite(pinList[i], HIGH);
  }
}

int menuButton() {  
  switch (lcd.button()) {
    case KEYPAD_DOWN:
      modus -= 1;
      if (modus <= 0) {
        modus = 4;
      }

      delay(300);
      break;

    case KEYPAD_UP:
      modus += 1;

      if (modus >= 5) {
        modus = 1;
      }
      delay(300);
      break;

    default:
      break;
  }

  printLCD();
  return modus;

}



//checks the speed read from left right button and adds val to it
int checkSpeed() {
  printLCD();

  int val = 50 ;

  if (lcd.button() == KEYPAD_LEFT ) {
    speedVal += val;
  }
  if (lcd.button() == KEYPAD_RIGHT ) {
    speedVal -= val;
  }
  if (speedVal <= 25) {
    speedVal = 25;
  }

  if (speedVal >= 10000) {
    speedVal = 10000;
  }

}


//schaltet alle an
void allOn() {
  for (int c = 0; c <= 17 ; c++) {

    digitalWrite(pinList[c], LOW);
  }
  allAreOn = true;
}

//schaltet alle aus
void allOff() {
  for (int c = 0; c <= 17 ; c++) {

    digitalWrite(pinList[c], HIGH);
  }
  allAreOn = false;
}

//Aktualisiert das LCD nachdem der timer abgelaufen ist.
void printLCD() {


  if (millis() > timeout + timer ) {
    timer = millis();
    //read DMX input and saves to array



    modusV = String(modus);
    speedV = String(speedVal);
    String line1 = ("modus: " + modusV);
    String line2 = ("speed: " + speedV);

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(line1);
    lcd.setCursor(0, 8);
    lcd.print(line2);
  }

}

void selectButtonCheck() {
  if (lcd.button() == KEYPAD_SELECT) {
    runProgram = false;
  }
  
}


//-------------------------------------------------------------------------------------
//----------------------------------  PROGRAMME    ------------------------------------
//-------------------------------------------------------------------------------------

void program1() { 
  int program =  menuButton();  
  while (runProgram == true && program == menuButton()) {
    for (int c = 0; c <= 17 ; c++) {
      //printLCD();
      digitalWrite(pinList[c], LOW);

      //speed abfrage
      checkSpeed();
      delay(speedVal);
      //select button abfrage.
      selectButtonCheck();
    }

    //abbruchbedingung in der 2ten for schleife
    if (runProgram != false) {
      for (int c = 0; c <= 16 ; c++) {
        //printLCD();
        digitalWrite(pinList[c], HIGH);
        checkSpeed();
        delay(speedVal);
      }
      selectButtonCheck();
    }

    if ( runProgram == false) {
      allOn();
      delay(3000);
      allOff();
    }

  }//if
  //schaltet alle aus wenn das programm verlassen wird.
  allOff();
}

void program2() {
  while (runProgram == true) {
    for (int c = 0; c <= 16 ; c++) {
      //
      digitalWrite(pinList[c], LOW);
      checkSpeed();
      delay(speedVal);
      digitalWrite(pinList[c], HIGH);
      //wenn select gedrueckt wird stoppt das programm
      selectButtonCheck();

    }//for

  }//while

  //nach abbruch 1sek zur bestaetigung.
  if ( runProgram == false) {
    allOn();
    delay(600);
    allOff();
  }
}

void program3() {
  while (runProgram == true) {
    int b = 15;
    for (int c = 0; c <= 16 ; c++) {

      digitalWrite(pinList[c], LOW);
      digitalWrite(pinList[b], HIGH);
      //ueberprueft ob button l oder r gedrueckt wurde
      checkSpeed();
      delay(speedVal);

      digitalWrite(pinList[c], HIGH);
      digitalWrite(pinList[b], LOW);
      delay(speedVal);
      b--;
      //printLCD();
      //wenn select gedrueckt wird stoppt das programm
      selectButtonCheck();
      if ( runProgram == false) {
        allOn();
        delay(1000);
        allOff();
      }
    }//for
  }//while

}

void program4() {
  if (allAreOn == false) {
    allOn();
    delay(200);
  }
  else {
    allOff();
    delay(200);
  }
}
